import React, { useEffect } from "react";
import store from "./redux/store";
import isLogin from "./lib/isLogin";
import { getAuthToken } from "./lib/localStorage";
import { decodeJwt } from "./lib/jsonwebtoken";
import HelperRoute from "./routeHelper/HelperRoute";
import AppRoutes from "./routeHelper/AppRoutes";
import Navigation from "./components/navigation/Navigation";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  const { auth } = store.getState().user;

  useEffect(() => {
    if (!auth && isLogin()) {
      decodeJwt(getAuthToken(), store.dispatch);
    }
  }, [auth]);

  return (
    <React.Fragment>
      <div className="tw-font-poppins">
        <Navigation />
        <ToastContainer />
        <HelperRoute />
        <AppRoutes />
      </div>
    </React.Fragment>
  );
}

export default App;
