import axios from "axios";
import { toast } from "react-toastify";

export const signinAPI = async (data) => {
  try {
    const respData = await axios({
        method: 'post',
        url: '/api/signin',
        data
    })

    return respData.data
  } catch (error) {
    toast.error(
      error.response.data.message ? error.response.data.message : error.message
    );
  }
};

export const postPatient = async (data) => {
  try {
    const respData = await axios({
      method: 'post',
      url: '/api/patient',
      data
  })

  return respData.data
  } catch (error) {
    toast.error(
      error.response.data.message ? error.response.data.message : error.message
    );
  }
}
