import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const Navigation = () => {
  const user = useSelector((state) => state.user);
  const navigate = useNavigate();
  return (
    <div
      className={user.auth ? "tw-w-full tw-relative" : "tw-w-full tw-absolute"}
    >
      <nav className="tw-bg-gray-800 tw-px-5 tw-py-3 tw-flex tw-justify-between tw-items-center tw-text-white ">
        <div>
          <p className="tw-text-xl">Hospital Insurance</p>
          <span className="tw-text-sm">Management system</span>
        </div>
        <div className="tw-flex tw-gap-x-5">
          {!user.auth && (
            <div
              className="tw-px-2 tw-py-1.5 tw-rounded hover:tw-bg-gray-200 hover:tw-text-gray-600 tw-cursor-pointer tw-duration-300"
              onClick={() => navigate("/signin")}
            >
              Sign In
            </div>
          )}
          {user.auth && (
            <div
              className="tw-px-2 tw-py-1.5 tw-rounded hover:tw-bg-gray-200 hover:tw-text-gray-600 tw-cursor-pointer tw-duration-300"
              onClick={() => navigate("/")}
            >
              Home
            </div>
          )}
          {user.auth && (
            <div
              className="tw-px-2 tw-py-1.5 tw-rounded hover:tw-bg-gray-200 hover:tw-text-gray-600 tw-cursor-pointer tw-duration-300"
              onClick={() => navigate("/add-patient")}
            >
              Add Patient
            </div>
          )}
          {user.auth && (
            <div
              className="tw-px-2 tw-py-1.5 tw-rounded hover:tw-bg-gray-200 hover:tw-text-gray-600 tw-cursor-pointer tw-duration-300"
              onClick={() => navigate("/signout")}
            >
              Sign Out
            </div>
          )}
        </div>
      </nav>
    </div>
  );
};

export default Navigation;
