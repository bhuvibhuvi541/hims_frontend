import axios from "axios";

export const removeAuthorization = () => {
    delete axios.defaults.headers.common["Authorization"];
}

export const setAuthorization = (token) => {
    axios.defaults.headers.common['Authorization'] = token;
}