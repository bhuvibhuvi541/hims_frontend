const isLogin = () => {
    if (localStorage.getItem('auth_token')) {
        return true;
    }
    return false;
}

export default isLogin;