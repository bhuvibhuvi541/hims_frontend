import jwt from "jwt-decode";
import { removeAuthorization } from "./authorization";
import { removeAuthToken } from "./localStorage";
import isEmpty from "./isEmpty";
import { initialize } from "../redux/slices/userSlice";

export const decodeJwt = (token, dispatch) => {
  try {
    if (!isEmpty(token)) {
      token = token.replace("Bearer ", "");
      const decoded = jwt(token);
      if (decoded) {
        dispatch(
          initialize({
            auth: true,
            userId: decoded.id,
            user: {}
          })
        );
      }
    }
  } catch (err) {
    removeAuthToken();
    removeAuthorization();
    dispatch(initialize({ auth: false, userId: "", user: {} }));
  }
};

