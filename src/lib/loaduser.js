import axios from "axios";
import { initialize } from "../redux/slices/userSlice";

export const loadUser = async (dispatch) => {
    try {
      const response = await axios({
        url: "/api/userinfo",
        method: "get",
      });
      dispatch(
        initialize({ auth: true, userId: response.data.user._id, user: response.data.user })
      );
    } catch (error) {
      dispatch(initialize({ auth: false }));
    }
  };
  