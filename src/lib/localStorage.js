export const getAuthToken = () => {
    if (localStorage.getItem("auth_token")) {
      return localStorage.getItem("auth_token");
    }
    return "";
  };
  
  export const removeAuthToken = () => {
    localStorage.removeItem("auth_token");
  };
  
  export const setAuthToken = async (token) => {
    // let expiredStorage = new ExpiredStorage();
    // expiredStorage.setItem("user_token", token, 43200); //43200 - 12hrs
    localStorage.setItem('auth_token', token);
    return true;
  };