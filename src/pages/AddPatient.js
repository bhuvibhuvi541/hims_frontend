import React, { useState } from "react";
import { toast } from "react-toastify";
import { postPatient } from "../api/admin";

const initialFormValue = {
  name: "",
  aadhaar: "",
  phoneNumber: "",
  door: "",
  street: "",
  town: "",
  city: "",
  district: "",
  state: "",
  location: "",
  company: "",
  startedOn: "",
  expires: "",
  coverage: "",
};
const AddPatient = () => {
  document.body.style.backgroundImage =
    'url("https://images.pexels.com/photos/7130555/pexels-photo-7130555.jpeg")';

  const [formValue, setFormValue] = useState(initialFormValue);

  const handleChange = (e) => {
    const { id, value } = e.target;
    let forms = { ...formValue, [id]: value };
    setFormValue({ ...forms });
  };

  const submitHandler = async () => {
    const reqData = {
      name: formValue.name,
      aadhaar: formValue.aadhaar,
      phoneNumber: formValue.phoneNumber,
      address: {
        door: formValue.door,
        street: formValue.street,
        town: formValue.town,
        city: formValue.city,
        district: formValue.district,
        state: formValue.state,
        location: formValue.location,
      },
      insurance: {
        company: formValue.company,
        startedon: formValue.startedOn,
        expires: formValue.expires,
        coverage: formValue.coverage,
      },
    };

    const { success, message } = await postPatient(reqData);
    if (success) {
      toast.success(message);
    }
  };

  return (
    <div className="container tw-mt-8 tw-mb-10">
      <h2 className="tw-text-2xl tw-font-semibold tw-text-slate-800 tw-mb-8">
        Add Patient
      </h2>
      <div className="tw-grid tw-grid-cols-3 tw-gap-4 tw-py-4 tw-mb-4">
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">Name</label>
          <br />
          <input
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full"
            type={"text"}
            onChange={handleChange}
            id="name"
            value={formValue.name}
          />
        </div>
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">Aadhaar</label>
          <br />
          <input
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full"
            type={"text"}
            onChange={handleChange}
            id="aadhaar"
            value={formValue.aadhaar}
          />
        </div>
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">Phone Number</label>
          <br />
          <input
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full"
            type={"tel"}
            onChange={handleChange}
            id="phoneNumber"
            value={formValue.phoneNumber}
          />
        </div>
      </div>
      <div className="tw-text-xl tw-font-semibold">Address</div>
      <div className="tw-grid tw-grid-cols-3 tw-gap-4 tw-py-4 tw-mb-4">
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">Door</label>
          <br />
          <input
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full"
            type={"text"}
            onChange={handleChange}
            id="door"
            value={formValue.door}
          />
        </div>
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">Street</label>
          <br />
          <input
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full"
            type={"text"}
            onChange={handleChange}
            id="street"
            value={formValue.street}
          />
        </div>
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">Town</label>
          <br />
          <input
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full"
            type={"text"}
            onChange={handleChange}
            id="town"
            value={formValue.town}
          />
        </div>
      </div>

      <div className="tw-grid tw-grid-cols-3 tw-gap-4 tw-py-4 tw-mb-4">
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">City</label>
          <br />
          <input
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full"
            type={"text"}
            onChange={handleChange}
            id="city"
            value={formValue.city}
          />
        </div>
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">District</label>
          <br />
          <input
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full"
            type={"text"}
            onChange={handleChange}
            id="district"
            value={formValue.district}
          />
        </div>
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">State</label>
          <br />
          <input
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full"
            type={"text"}
            onChange={handleChange}
            id="state"
            value={formValue.state}
          />
        </div>
      </div>

      <div className="tw-grid tw-grid-cols-3 tw-gap-4 tw-py-4 tw-mb-4">
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">Locality</label>
          <br />
          <select
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full tw-bg-white"
            onChange={handleChange}
            id="location"
            value={formValue.location}
          >
            <option>Select</option>
            <option value={"Urban"}>Urban</option>
            <option value={"Sub-Urban"}>Sub - Urban</option>
            <option value={"Rural"}>Rural</option>
          </select>
        </div>
      </div>
      <div className="tw-text-xl tw-font-semibold">Insurance</div>
      <div className="tw-grid tw-grid-cols-3 tw-gap-4 tw-py-4 tw-mb-4">
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">Company</label>
          <br />
          <input
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full"
            type={"text"}
            onChange={handleChange}
            id="company"
            value={formValue.company}
          />
        </div>
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">Started On</label>
          <br />
          <input
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full"
            type={"date"}
            onChange={handleChange}
            id="startedOn"
            value={formValue.startedOn}
          />
        </div>
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">Expires On</label>
          <br />
          <input
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full"
            type={"date"}
            onChange={handleChange}
            id="expires"
            value={formValue.expires}
          />
        </div>
      </div>
      <div className="tw-grid tw-grid-cols-3 tw-gap-4 tw-py-4 tw-mb-4">
        <div className="tw-col-span-1">
          <label className="tw-mb-1 tw-text-lg">Coverage</label>
          <br />
          <input
            className="tw-px-4 tw-py-1.5 tw-rounded-full tw-w-full"
            type={"number"}
            onChange={handleChange}
            id="coverage"
            value={formValue.coverage}
          />
        </div>
      </div>
      <div className="tw-flex tw-justify-end tw-items-center tw-mb-10">
        <button
          onClick={submitHandler}
          className="tw-px-3 tw-py-1.5 tw-rounded-xl tw-bg-teal-500 tw-text-white tw-border-teal-500 hover:tw-bg-white hover:tw-text-teal-500 tw-duration-300"
        >
          Add Patient
        </button>
      </div>
    </div>
  );
};

export default AddPatient;
