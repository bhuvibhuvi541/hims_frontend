import React from "react";
import DTView from "../components/Dataview/DTview";

const Home = () => {
  return (
    <div className="container">
      <div className="tw-mt-5 tw-text-lg">
        Hello, Welcome Hospital Institute Management Project,...
      </div>
      <div className="">

      </div>
      <DTView />
    </div>
  );
};

export default Home;
