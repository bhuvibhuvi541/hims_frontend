import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { signinAPI } from "../api/admin";
import { setAuthorization } from "../lib/authorization";
import { loadUser } from "../lib/loaduser";
import { setAuthToken } from "../lib/localStorage";
import { initialize } from "../redux/slices/userSlice";

const initialFormValue = {
  email: "",
  password: "",
};

const SignIn = () => {
  const [formValue, setFormValue] = useState(initialFormValue);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const changeHandler = (e) => {
    let { id, value } = e.target;
    formValue[id] = value;
    setFormValue({ ...formValue });
  };

  const submitHandler = async () => {
    try {
      const { success, message, token } = await signinAPI(formValue);
      if (success) {
        toast.success(message);
        setAuthorization(token);
        // loadUser(dispatch)
        dispatch(initialize({ auth: true }));
        setAuthToken(token);
        navigate("/");
      }
    } catch (error) {
      toast.error("Error on Sign in");
    }
  };

  return (
    <div
      className="tw-w-full tw-flex tw-justify-center tw-items-center tw-bg-no-repeat"
      style={{
        height: "100vh",
        backgroundImage:
          'url("https://images.pexels.com/photos/7130555/pexels-photo-7130555.jpeg")',
        backgroundSize: "100% 100%",
      }}
    >
      <div className="tw-w-1/4 tw-p-6 tw-rounded-lg tw-bg-white tw-bg-opacity-60">
        <h3 className="tw-text-xl tw-mb-4 tw-font-semibold">Sign In</h3>
        <div className="tw-w-full">
          <div className="tw-w-full tw-mb-4">
            <label className="tw-mb-1.5">Email</label>
            <input
              className="tw-w-full tw-p-2 tw-px-4 tw-rounded-full tw-text-sm"
              type={"email"}
              name="email"
              id="email"
              value={formValue.email}
              onChange={changeHandler}
            />
          </div>
          <div className="tw-w-full tw-mb-4">
            <label className="tw-mb-1.5">Password</label>
            <input
              className="tw-w-full tw-p-2 tw-px-4 tw-rounded-full tw-text-sm"
              type={"password"}
              name="password"
              id="password"
              value={formValue.password}
              onChange={changeHandler}
            />
          </div>
          <div className="tw-w-full tw-flex tw-justify-end tw-items-center">
            <button
              className="tw-px-3 tw-py-1.5 tw-rounded-xl tw-bg-teal-500 tw-text-white tw-border-teal-500 hover:tw-bg-white hover:tw-text-teal-500 tw-duration-300"
              onClick={submitHandler}
            >
              Sign In
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignIn;
