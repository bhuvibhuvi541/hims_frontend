import React, { useEffect } from "react";
import { removeAuthorization } from "../lib/authorization";
import { removeAuthToken } from "../lib/localStorage";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { initialize } from "../redux/slices/userSlice";

const Signout = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    removeAuthToken();
    removeAuthorization();
    dispatch(initialize({ auth: false, userId: "", user: {} }));
    navigate("/")
  }, []);
  return <div></div>;
};

export default Signout;
