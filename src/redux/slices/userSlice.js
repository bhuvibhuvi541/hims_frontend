import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  auth: false,
  userId: '',
  user: {},
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    initialize: (state, action) => {
      state.auth = action.payload.auth;
      state.userId = action.payload.userId;
      state.user = action.payload.user;
    },
  },
});

export const { initialize } = userSlice.actions;
export default userSlice.reducer;
