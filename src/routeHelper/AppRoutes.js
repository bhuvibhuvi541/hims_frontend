import React from "react";
import { useRoutes } from "react-router-dom";
import PatientInfo from "../pages/PatientInfo";
import { ProtectedRoute } from "./ProtectedRoute";
import Home from "../pages/Home";
import SignIn from "../pages/SignIn";
import Signout from "../pages/Signout";
import AddPatient from "../pages/AddPatient";

const routes = [
  {
    path: "/",
    element: (
      <ProtectedRoute type={"private"}>
        <Home />
      </ProtectedRoute>
    ),
  },
  {
    path: "/signin",
    element: (
      <ProtectedRoute type={"auth"}>
        <SignIn />
      </ProtectedRoute>
    ),
  },
  {
    path: "/signout",
    element: (
      <ProtectedRoute type={"private"}>
        <Signout />
      </ProtectedRoute>
    ),
  },
  {
    path: "/add-patient",
    element: (
      <ProtectedRoute type={"private"}>
        <AddPatient />
      </ProtectedRoute>
    ),
  },
  {
    path: "/patientInfo/:id",
    element: (
      <ProtectedRoute type={"private"}>
        <PatientInfo />
      </ProtectedRoute>
    ),
  },
  {
    path: "/*",
    element: <>404</>,
  },
];

const AppRoutes = () => {
  return <>{useRoutes(routes)}</>;
};

export default AppRoutes;
