import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { loadUser } from '../lib/loaduser';

const HelperRoute = () => {
    const dispatch = useDispatch();
    const { auth } = useSelector(state => state.user);
    useEffect(() => {
        if (auth) {
            loadUser(dispatch)
        }
    }, [auth])
  return <React.Fragment />;
};

export default HelperRoute;